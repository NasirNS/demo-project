from django.db import models


# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True, default=None)
    email = models.EmailField(max_length=255, null=True, blank=True, default=None)
    username = models.CharField(max_length=255, null=True, blank=True, default=None)
    designation = models.CharField(max_length=255, null=True, blank=True, default=None)
    address = models.CharField(max_length=255, null=True, blank=True, default=None)
    contact_no = models.IntegerField()
