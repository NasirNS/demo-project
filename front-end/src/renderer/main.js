import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import Vuetify, {VApp, VContainer, VCheckbox, VRow, VCol, VCard, VCardTitle, VSpacer, VBtn, VSnackbar, VDataTable, VPagination, VTooltip, VDialog, VTextField, VCardText, VForm, VCardActions, VIcon} from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(Vuetify, {
  components: {
    VApp,
    VContainer,
    VRow,
    VCol,
    VCard,
    VCardTitle,
    VSpacer,
    VBtn,
    VSnackbar,
    VDataTable,
    VPagination,
    VTooltip,
    VCheckbox,
    VIcon,
    VDialog,
    VTextField,
    VForm,
    VCardActions,
    VCardText
  }
})
const opts = {
  theme: {
    dark: false
  }
}
/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>',
  vuetify: new Vuetify(opts)
}).$mount('#app')
