import Rules from '../../Rules'
import EmployeeFiels from '../../componentsFiles/employeeFiles'

export default {
  props: ['isDialogOpen', 'editindex', 'editItem'],
  mixins: [EmployeeFiels],
  extends: Rules,
  data: function () {
    return {
      formDialog: false,
      editedIndex: -1,
      editedItem: {},
      defaultItem: {},
      snackBar: {
        visibility: false,
        color: 'success',
        mode: '',
        timeout: 2000,
        text: ''
      }
    }
  },
  created: function () {
    this.initModel()
  },
  computed: {},
  watch: {
    isDialogOpen: function () {
      this.editedIndex = this.editindex
      this.editedItem = this.editItem
      this.formDialog = this.isDialogOpen
    }
  },
  methods: {
    initModel: function () {
      this.editedItem = {
        name: '',
        username: '',
        email: '',
        designation: '',
        contact_no: '',
        address: ''
      }
      this.defaultItem = Object.assign({}, this.editedItem)
    }
  }
}
