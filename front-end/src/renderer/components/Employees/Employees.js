import AddEmployees from '../Employees/add/index'
import EmployeeFiels from '../componentsFiles/employeeFiles'

export default {
  components: {
    AddEmployees
  },
  mixins: [EmployeeFiels],
  data () {
    return {
      dialogOpening: false,
      editedIndex: -1,
      editedItem: {},
      status: false,
      defaultItem: {},
      headers: [
        { text: 'Full Name', sortable: true, value: 'name' },
        { text: 'Username', sortable: true, value: 'username' },
        { text: 'Email', sortable: true, value: 'email' },
        { text: 'Designation', sortable: true, value: 'designation' },
        { text: 'Contact #', sortable: true, value: 'contact_no' },
        { text: 'Address', sortable: true, value: 'address' },
        { text: 'Actions', value: 'action', sortable: false }
      ]
    }
  },
  created: function () {
    this.getEmployees()
  },
  methods: {
    eventChild: function () {
      this.getEmployees()
    },
    closeEvent: function () {
      this.dialogOpening = false
    }
  }
}
