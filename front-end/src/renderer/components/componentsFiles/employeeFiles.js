
import api from '../../services/model'
import axios from 'axios'
import MainEmployeeFile from '../Employees/Employees'

export default {
  mixins: MainEmployeeFile,
  data () {
    return {
      loading: true,
      EmployeeList: [],
      DeleteSlotModal: false,
      dialogOpening: false,
      snackBar: {
        visibility: false,
        color: 'success',
        mode: '',
        timeout: 2000,
        text: ''
      }
    }
  },
  methods: {
    offlineSaveData () {
      let query = 'select * from `tbl_employees` where id = last_insert_rowid()'
      if (this.editedIndex > -1) {
        if (this.$refs.EmployeeForm.validate()) {
          let keyValue = {
            columns: Object.keys(this.editedItem),
            values: Object.values(this.editedItem)
          }
          api.saveFormData('tbl_employees', keyValue, query, (err, row) => {
            if (err) throw err
            else {
              this.closeForm()
              setTimeout(() => {
                this.$emit('event_child')
                this.showCustomNotification('success', row.name + ' updated successfully')
              }, 750)
            }
          })
        }
      } else {
        delete this.editedItem.id
        if (this.$refs.EmployeeForm.validate()) {
          let keyValue = {
            columns: Object.keys(this.editedItem),
            values: Object.values(this.editedItem)
          }
          api.saveFormData('tbl_employees', keyValue, query, (err, row) => {
            if (err) throw err
            else {
              this.closeForm()
              setTimeout(() => {
                this.$emit('event_child')
                this.showCustomNotification('success', row.name + ' added successfully')
              }, 650)
            }
          })
        }
      }
    },
    onlineSaveData () {
      console.log(this.editedItem)
      if (this.editedIndex > -1) {
        axios.post('http://127.0.0.1:8000/api/', this.editedItem).then((response) => {
          this.closeForm()
          setTimeout(() => {
            this.$emit('event_child')
            this.showCustomNotification('success', response.data.name + ' added successfully')
          }, 650)
        })
      } else {
        axios.put(`http://127.0.0.1:8000/api/${this.editedItem.id}`, this.editedItem).then((response) => {
          debugger
          this.closeForm()
          setTimeout(() => {
            this.$emit('event_child')
            this.showCustomNotification('success', response.data.name + ' updated successfully')
          }, 650)
        })
      }
    },
    saveFormData () {
      if (!this.status) {
        this.offlineSaveData()
      } else {
        this.onlineSaveData()
      }
    },
    updateItem (item) {
      this.editedIndex = this.EmployeeList.indexOf(item)
      this.editedItem = Object.assign({}, item)
      this.dialogOpening = true
    },
    deleconfirmation (item) {
      this.DeleteSlotModal = true
      this.editedItem = Object.assign({}, item)
    },
    deleteOfflineItem: function () {
      const index = this.EmployeeList.indexOf(this.editedItem)
      const response = api.deleteItem('tbl_employees', this.editedItem.id)
      if (!response.isError) {
        this.EmployeeList.splice(index, 1)
        this.DeleteSlotModal = false
        this.$emit('event_child')
        this.showCustomNotification('success', ' Deleted successfully')
      }
    },
    deleteOnlineItem: function () {
      axios.delete(`http://127.0.0.1:8000/api/${this.editedItem.id}`).then(() => {
        this.DeleteSlotModal = false
        this.$emit('event_child')
        this.showCustomNotification('success', ' Deleted successfully')
      })
    },
    deleteItem () {
      if (this.status) {
        this.deleteOnlineItem()
      } else {
        this.deleteOfflineItem()
      }
    },
    getOfflineEmployees: function () {
      this.loading = true
      const query = 'select * from tbl_employees'
      api.getAllData(query, (err, rows) => {
        if (err) throw err
        else {
          this.EmployeeList = rows
        }
      })
      this.loading = false
    },
    getOnlineEmployees: function () {
      this.loading = true
      axios.get('http://127.0.0.1:8000/api').then((respose) => {
        this.EmployeeList = respose.data
      })
      this.loading = false
    },
    getEmployees: function () {
      if (this.status) {
        this.getOnlineEmployees()
      } else {
        this.getOfflineEmployees()
      }
    },
    closeForm () {
      this.$emit('close')
      this.formDialog = false
      setTimeout(() => {
        this.editedItem = Object.assign({}, this.defaultItem)
        this.editedIndex = -1
        this.status = false
        this.$refs.EmployeeForm.resetValidation()
      }, 300)
    },
    showCustomNotification: function (status, message) {
      this.snackBar.color = status
      this.snackBar.text = message
      this.snackBar.visibility = true
    }
  }
}
