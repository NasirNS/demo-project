export default {
  data () {
    return {
      taxRules: [],
      priceRules: [
        v => !!v || 'This field is required',
        v => /^[+]?\d+(\.\d{2,20})?$/.test(v) || 'Subtotal Price is not valid'
      ],
      cardRules: [],
      quantityRules: [
        v => !!v || 'This field is required',
        v => /^[+]?\d+(\.\d+)?$/.test(v) || 'Quantity is not valid'
      ],
      discountRules: [
        v => !!v || 'This field is required',
        v => /^[+]?\d+(\.\d{2,4})?$/.test(v) || 'Discounted Price is not valid'
      ],
      RequiredRule: [
        v => !!v || 'This field is required'
      ],
      cnicRules: [
        v => !!v || 'This field is required'
      ],
      integerRules: [
        v => !!v || 'This field is required',
        v => /^\d*[1-9]\d*$/.test(v) || 'Enter Only Integers'
      ],
      designationRules: [
        v => !!v || 'This field is required',
        v => /^s*[a-zA-Z.\s]+\s*$/.test(v) || 'Designation must be valid'
      ],
      PassportRules: [
        v => /^([A-Z]{1}\d{8})?$/.test(v) || 'Passport must be 1 capital letter and 8 digits'
      ],
      LoginUserNameRules: [
        v => !!v || 'Username or Email is required'
      ],
      UserNameRules: [
        v => !!v || 'This field is required',
        v => /^\w(?:\w*(?:[.-]\w+)?)*(?<=^.{4,32})$/.test(v) || 'Username must be valid'
      ],
      nameRules: [
        v => !!v || 'This field is required',
        v => /^[A-Z][A-Za-z_-\s]{0,19}$/.test(v) || 'First letter should be capital'
      ],
      ProdNameRule: [
        v => !!v || 'This field is required',
        v => /^[A-Z][0-9A-Za-z_-\s]{0,50}$/.test(v) || 'First letter should be capital'
      ],
      EmailRules: [
        v => !!v || 'This field is required',
        v => /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v) || 'E-mail must be valid'
      ],
      PhoneRules: [
        v => !!v || 'This field is required',
        v => /^(([(]?[+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\\.]?[(]?[0-9]{1,3}[)]?([-\s\\.]?[0-9]{3})([-\s\\.]?[0-9]{3,4})$/.test(v) || 'Please enter a valid contact number'
      ],
      AddressRules: [
        v => !!v || 'This field is required',
        v => (v && v.length <= 500) || 'Address must be less than 500 characters'
      ],
      NotRequiredRules: [
        v => !v || 'not required'
      ],
      reqRules: [
        v => !!v || 'This field is required'
      ],
      amountRules: [
        v => !!v || 'This field is required',
        v => /^[+]?\d+(\.\d+)?$/.test(v) || 'Quantity is not valid'
      ],
      dateRules: [
        v => !!v || 'This field is required'
      ],
      numberRules: [
        v => !!v || 'This field is required',
        v => /^[+-]?\d+(\.\d{2,4})?$/.test(v) || 'Price is not valid'
      ],
      numbersRules: [
        v => !!v || 'This field is required',
        v => /^[^\d]*?(\d|\d[^\d]+){0,10}$/.test(v) || 'Price is not valid'
      ],
      currencyRules: [
        v => !!v || 'This field is required',
        v => /^[a-zA-Z\s]+$/.test(v) || 'Numbers & Special characters are not allowed'
      ],
      isoCodeRules: [
        v => !!v || 'This field is required',
        v => /^[A-Z]{3}$/.test(v) || 'Capital Alphabets of length 3'
      ],
      isoSymbolRules: [
        v => !!v || 'This field is required'
      ],
      baseCurrencyRules: [
        v => !!v || 'This field is required'
      ],
      descriptionRules: [
        v => !!v || 'This field is required'
      ],
      costPriceRules: [
        v => !!v || 'This field is required',
        v => /^[+]?\d+(\.\d{0,2})?$/.test(v) || 'Cost Price is not valid'
      ],
      ProductnameRules: [
        v => !!v || 'This field is required'
      ],
      skuRules: [
        v => !!v || 'This field is required'
      ],
      passwordRules: [
        v => !!v || 'Password is required'
      ],
      PasswordRules: [
        v => /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(v) || 'This field is required',
        v => (v && v.length >= 8) || 'Minimum 8 characters, at least one letter and one number:'
      ],
      PasswordRuless: [
        v => (v && v.length >= 8) || 'Minimum 8 characters, at least one letter and one number:'
      ],

      signuppasswordRules: [
        v => /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$$/.test(v) || 'Password must include at least one upper case letter, one lower case letter, and one numeric digit'
      ],
      promotionRules: [
        v => !!v || 'This field is required',
        v => /^[+-]?\d+(\.\d{2,4})?$/.test(v) || 'Promotion is not valid'
      ],
      StatusRules: [
        v => !!v || 'This field is required'
      ],
      accessRightsRules: [
        v => !!v || 'This field is required'
      ],
      userNameRules: [
        v => !!v || 'This field is required',
        v => /^\w(?:\w*(?:[.-]\w+)?)*(?<=^.{4,32})$/.test(v) || 'Minimum 4 characters. No white space allowed. Allowed characters are alphabets (a-z), digits (0-9), underscore (_), hyphen (-) , and period (.)'
      ],
      emailRules: [
        v => !!v || 'This field is required',
        v => /^(([^<>()\\[\]\\.,;:\s@"]+(\.[^<>()\\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v) || 'E-mail must be valid'
      ],
      inputRules: [
        v => v.length <= 0 || 'Prove some value.'
      ],
      NameRules: [
        v => !!v || 'This field is required',
        v => /^[a-zA-Z\s]+$/.test(v) || 'Numbers & Special characters are not allowed'
      ],
      selectRules: [
        v => !!v || 'This field is required'
      ],
      mask: {
        cardNoMask: '####-####-####-####',
        cnicMask: '#####-#######-#',
        priceMask: '##########',
        cardMonthMask: '##',
        cardYearMask: '####',
        cardCvvMask: '###',
        timeMask: '##:##',
        re_orderMask: '####'
      }
    }
  },
  computed: {
    changeCardRules: function () {
      for (let i = 0; i <= this.Cards.length; i++) {
        if (this.card.pmc === 'Visa') {
          this.cardRules = [
            v => /(^(4[0-9]{3}))((-[0-9]{4}){3})$/.test(v) || 'Please enter a valid Visa number'
          ]
        } else if (this.card.pmc === 'Master Card') {
          this.cardRules = [
            v => /(^(5[1-5][0-9]{2}))((-[0-9]{4}){3})$/.test(v) || 'Please enter a valid Master Card number'
          ]
        } else if (this.card.pmc === 'Amex') {
          this.mask.cardNoMask = '####-######-#####'
          this.cardRules = [
            v => /(^(3[47][0-9]{2}))(-[0-9]{6})(-[0-9]{5})$/.test(v) || 'Please enter a valid Amex number'
          ]
        } else {
          this.cardRules = [
            v => /^(6(?:011|5[0-9][0-9]))((-[0-9]{4}){3})$/.test(v) || 'Please enter a valid Discover number'
          ]
        }
        return this.cardRules
      }
    },
    updateUserPasswordConfirmationRules: function () {
      return [
        () => (this.updatedPass === this.confirmPassword) || 'Password must match',
        v => !!v || 'This field is required',
        v => (v && v.length >= 6) || 'Password must be at least 6 characters'
      ]
    },
    currentUserPasswordConfirmationRules: function () {
      return [
        () => (this.verifyCurrentPassword === this.editedItem.password) || 'Password must match',
        v => !!v || 'This field is required'
      ]
    },
    newUserPasswordConfirmationRules: function () {
      return [
        () => (this.editedItem.password === this.confirmPassword) || 'Password must match',
        v => !!v || 'This field is required',
        v => (v && v.length >= 6) || 'Password must be at least 8 characters'
      ]
    }
  },
  methods: {
    selectTaxType: function () {
      if (this.editedItem.taxType === '%') {
        this.taxRules = [
          v => /^[1-9][0-9]?$|^100$/.test(v) || 'enter only 3 digits between 1 to 100'
        ]
      } else {
        this.taxRules = [
          v => /^[0-9]{1,6}$/.test(v) || 'enter valid amount'
        ]
      }
    }
  }
}
