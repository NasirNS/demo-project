var sqlite3 = require('sqlite3').verbose()

var db = new sqlite3.Database('crud.db', (err) => {
  if (err) {
    return console.error(err.message)
  }
  console.log('Connected to the in-memory SQlite database.')
})

const query = 'CREATE TABLE tbl_employees (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT, email TEXT, designation TEXT, username TEXT, address TEXT, contact_no INTEGER)'
const readSchema = function () {
  db.serialize(() => {
    db.run(query, (err) => {
      if (err) {
        console.log(err)
      }
    })
  })
}

const saveFormData = function (tableName, keyValue, getItemQuery, callback) {
  let query = 'INSERT OR REPLACE INTO `' + tableName + '` (`' + keyValue.columns.join('`, `') + '`) VALUES (' + _placeHoldersString(keyValue.values.length) + ')'
  let statement = db.prepare(query)
  statement.run(keyValue.values)
  statement.finalize()
  getData(getItemQuery, callback)
}

const getAllData = function (query, callback) {
  db.all(query, callback)
}

const getData = function (query, callback) {
  db.get(query, callback)
}
let _placeHoldersString = function (length) {
  let places = ''
  for (let i = 1; i <= length; i++) {
    places += '?, '
  }
  return /(.*),/.exec(places)[1]
}

const deleteItem = function (tableName, id) {
  let payload = []
  let isError = false
  let query = 'DELETE FROM `' + tableName + '` WHERE `id` = ?'
  let statement = db.prepare(query)
  try {
    if (statement.run([id])) {
      payload = statement
      isError = false
      return { 'payload': payload, 'isError': isError }
    } else {
      payload = 'No data found for person_id = ' + id
      isError = true
      return { 'payload': payload, 'isError': isError }
    }
  } catch (error) {
    console.log(error)
  }
}
export default {
  readSchema,
  getAllData,
  saveFormData,
  deleteItem
}
