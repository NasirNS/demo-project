CREATE TABLE IF NOT EXISTS "tbl_employees" (
     "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
     "full_name" VARCHAR(50) NOT NULL,
     "username" VARCHAR(100) NOT NULL,
     "contact" VARCHAR(14) NULL,
     "address" TEXT(255,0) NULL,
     "email" VARCHAR(100) NULL,
     "contact" VARCHAR(12) NULL,
     "designation" VARCHAR(100) NOT NULL
);